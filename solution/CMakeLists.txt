file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    include/*.h
)

add_executable(image-transformer ${sources}
        src/angle.c
        include/angle.h
        include/bmp.h
        src/bmp.c
        include/image.h
        include/image.h

)
target_include_directories(image-transformer PRIVATE src include)
