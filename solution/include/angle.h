//
// Created by nikitosek on 19.11.2023.
//

#ifndef IMAGE_TRANSFORMER_ANGLE_H
#define IMAGE_TRANSFORMER_ANGLE_H
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

#define ANGLES_COUNT 7
#define RADIX 10
static const int32_t angles_list[] = {0, 90, -90, 180, -180, 270, -270};
bool is_right_angle(const char *user_angle, long int *angle);
#endif //IMAGE_TRANSFORMER_ANGLE_H
