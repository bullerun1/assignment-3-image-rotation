//
// Created by nikitosek on 26.11.2023.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#define BASE_ANGLE_OF_ROTATION 90
#include <inttypes.h>
struct pixel {
    uint8_t r, g, b;
};
struct image {
    uint64_t width, height;
    struct pixel *data;
};
void rotate(struct image* img, long int angle);
#endif //IMAGE_TRANSFORMER_IMAGE_H
