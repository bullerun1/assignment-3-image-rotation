//
// Created by nikitosek on 19.11.2023.
//
#include <angle.h>
#include <stdlib.h>

bool is_right_angle(const char *user_angle, long int *angle){
    char *endptr;
    *angle = strtol(user_angle, &endptr, RADIX);
    // Проверяем, является ли аргумент целым числом
    if (*endptr != '\0') {
        perror("The angle is not an integer.");
        return false;
    } else {
        for (int i = 0; i < ANGLES_COUNT; ++i) {
            if(*angle == angles_list[i]) return true;
        }
        perror("The angle can take values strictly from the list: 0, 90, -90, 180, -180, 270, -270");
        return false;
    }
}



