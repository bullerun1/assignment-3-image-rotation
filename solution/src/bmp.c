//
// Created by nikitosek on 26.11.2023.
//

#include "bmp.h"
#include <stdlib.h>

static uint8_t paddingCounter(uint64_t width) {
    return PADDING_IN_BMP - width * sizeof(struct pixel) % PADDING_IN_BMP;
}

static struct pixel *read_pixels(FILE *input_file, uint64_t const height, uint64_t const biWidth) {
    struct pixel *pixels = malloc(sizeof(struct pixel) * height * biWidth);
    for (size_t i = 0; i < height; i++) {
        if (fread(pixels + i * biWidth, sizeof(struct pixel), biWidth, input_file) != biWidth) {
            free(pixels);
            return 0;
        }
        fseek(input_file, paddingCounter(biWidth), SEEK_CUR);
    }
    return pixels;
}


bool from_bmp(FILE *input_file, struct image *img) {
    struct bmp_header *header = malloc(sizeof(struct bmp_header));
    if (fread(header, sizeof(struct bmp_header), 1, input_file) != 1) {
        free(header);
        return false;
    };
    img->height = header->biHeight;
    img->width = header->biWidth;
    img->data = read_pixels(input_file, img->height, img->width);
    if (!img->data) {
        free(header);
        return false;
    }
    free(header);
    return true;
}

void create_header(struct bmp_header *header, struct image *image) {
    uint8_t padding = paddingCounter(image->width);
    header->bfType = 0x4D42;
    header->bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * (image->width * padding) * image->height;
    header->bfReserved = 1;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = 40;
    header->biWidth = image->width;
    header->biHeight = image->height;
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biCompression = 0;
    header->biSizeImage = (image->width + padding) * image->height;
    header->biXPelsPerMeter = 1;
    header->biYPelsPerMeter = 1;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
}


bool to_bmp(FILE *output_file, struct image *img) {
    struct bmp_header *header = malloc(sizeof(struct bmp_header));
    create_header(header, img);
    fwrite(header, sizeof(struct bmp_header), 1, output_file);
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, output_file) != img->width) {
            free(header);
            return false;
        };
        fseek(output_file, paddingCounter(img->width), SEEK_CUR);
    }
    free(header);
    return true;
}


