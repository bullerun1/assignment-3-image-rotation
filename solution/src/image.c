//
// Created by nikitosek on 26.11.2023.
//
#include "bmp.h"
#include "image.h"

void rotate(struct image *img, long int angle) {
    long int number_rotations;
    if (angle >= 0) {
        number_rotations = angle / BASE_ANGLE_OF_ROTATION;
    } else {
        number_rotations = (360 + angle) / BASE_ANGLE_OF_ROTATION;
    }


    while (number_rotations) {
        struct image *img_copy = malloc(sizeof(struct image));
        img_copy->data = malloc(sizeof (struct pixel)* img->height * img->width);
        img_copy->width = img->height;
        img_copy->height = img->width;
        for (size_t i = 0; i < img_copy->height; i++)
            for (size_t j = 0; j < img_copy->width; j++)
                img_copy->data[i * img_copy->width + j] = img->data[j * img->width + img->width - i - 1];
        free(img->data);
        img->data = img_copy->data;
        img->width = img_copy->width;
        img->height = img_copy->height;
        free(img_copy);
        number_rotations--;
    }

}

