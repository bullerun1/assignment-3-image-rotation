#include "angle.h"
#include "bmp.h"
#include "image.h"

bool is_correct_args_count(int32_t argc) {
    if (argc != 4) {
        perror("Invalid number of arguments");
        return false;
    }
    return true;
}

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;
    if (!is_correct_args_count(argc)) return 128;


    long int angle;
    if (!is_right_angle(argv[3], &angle)) return 128;

    FILE *input_file = fopen(argv[1], "rb");
    if (!input_file) {
        perror("Could not open file for reading");
        return 132;
    }
    struct image *img = malloc(sizeof(struct image));
    if (!from_bmp(input_file, img)){
        fclose(input_file);
        free(img->data);
        free(img);
        return 1;
    };
    fclose(input_file);


    rotate(img, angle);

    FILE *output_file = fopen(argv[2], "wb");
    if (!output_file) {
        perror("Could not open the file for writing");
        free(img->data);
        free(img);
        return 132;
    }
    if(!to_bmp(output_file, img)){
        fclose(output_file);
        free(img->data);
        free(img);
        return 1;
    };
    fclose(output_file);
    free(img->data);
    free(img);
    printf("The image has been successfully rotated and saved to %s", argv[2]);

    return 0;

}
